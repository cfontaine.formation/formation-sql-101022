USE bibliotheque;

-- Jointure interne => INNER JOIN
SELECT titre, nom ,livres.genre , genres.id FROM livres, genres WHERE livres.genre = genres.id;

SELECT livres.id, titre, annee, nom FROM livres
INNER JOIN genres ON livres.genre = genres.id;

SELECT prenom,auteurs.nom,naissance, pays.nom AS nation FROM auteurs
INNER JOIN pays ON auteurs.nation =pays.id;

SELECT prenom,auteurs.nom,naissance, pays.nom AS nation FROM auteurs
INNER JOIN pays ON auteurs.nation =pays.id
WHERE pays.nom='france';

SELECT prenom, auteurs.nom ,titre, genres.nom FROM auteurs
INNER JOIN livre2auteur ON auteurs.id= livre2auteur.id_auteur
INNER JOIN livres ON livres.id=livre2auteur.id_livre 
INNER JOIN genres ON livres.genre =genres.id;

SELECT titre, genres.nom  ,prenom, auteurs.nom FROM auteurs
INNER JOIN livre2auteur ON auteurs.id= livre2auteur.id_auteur
INNER JOIN livres ON livres.id=livre2auteur.id_livre 
INNER JOIN genres ON livres.genre =genres.id
WHERE deces IS NULL ORDER BY auteurs.nom, annee;

-- CROSS JOIN
USE exemple;
CREATE TABLE plats (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO plats(nom) VALUES 
('céréale'),
( 'oeuf sur le plat'),
('pain');

CREATE TABLE boissons(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30)
);

INSERT INTO  boissons(nom) VALUES('café'),('thé'), ('jus d\'orange');

SELECT plats.nom AS plat,boissons.nom AS boisson FROM plats
CROSS JOIN boissons;

USE bibliotheque ;

-- Jointure externe
SELECT nom , titre FROM genres LEFT JOIN livres ON livres.genre= genres.id;

SELECT nom FROM genres LEFT JOIN livres ON livres.genre = genres.id WHERE livres.id IS NULL;

SELECT nom , titre FROM livres RIGHT JOIN genres ON livres.genre =genres.id 

SELECT nom FROM livres RIGHT JOIN genres ON livres.genre =genres.id  WHERE livres.id IS NULL;

SELECT nom, titre FROM livres
FULL JOIN genres ON genre= genres.id;

-- Jointure naturelle
ALTER TABLE genres CHANGE id genre INT;

SELECT titre, nom FROM livres NATURAL JOIN genres; 

ALTER TABLE genres CHANGE  genre  id INT;

-- Auto-jointure
USE exemple;

CREATE TABLE salaries(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR (50),
	nom VARCHAR (50),
	manager INT,
	
	CONSTRAINT fk_manager
	FOREIGN KEY (manager)
	REFERENCES salaries (id)
);

INSERT INTO salaries (prenom, nom,manager) VALUES 
('JOHN','DOE',NULL),
('Alan','Smithee',1),
('Jo','Dalton',1),
('wiliam','dalton',3),
('Jack','dalton',3);

SELECT employe.prenom ,employe.nom ,manager.prenom,manager.nom 
FROM salaries AS employe
LEFT JOIN salaries AS manager ON employe.manager =manager.id

