/*01 -Conception de base */

-- Suprimer la table exemple, si elle existe
DROP DATABASE IF EXISTS exemple;

-- Créer une base de donnée exemple
CREATE DATABASE  exemple;

-- CREATE DATABASE IF NOT EXISTS exemple;

-- Afficher toutes les bases de données du serveur
SHOW DATABASES;

-- Choisir exemple comme base de données courante
USE exemple;

-- Créer une TABLE articles qui contient 3 colonnes reférence, description et prix
CREATE TABLE articles(
	reference INT COMMENT 'reférence de l''article' , --  COMMENT -> Ajout d'un commentaire sur la colonne
	description VARCHAR(255) UNIQUE, -- UNIQUE -> Il ne peut pas avoir de doublon pour la description du produit 
	prix DOUBLE	 NOT NULL DEFAULT 10 -- NOT NULL -> prix ne peut plus être égal à NULL, lorsque l'on ajoutera des données, on sera obligé de données un valeur à prix
									 -- DEFAULT -> permet de définir une valeur par défaut autre que NULL
)ENGINE=InnoDB;						

-- ENGINE -> Choix du moteur de base de données
-- par défaut InnoDB ->  supporte les transactions et les clés étrangère
-- autre moteur:
--  MEMORY -> stockage en mémoire
-- MyISAM  -> ancien moteur, par défaut pas de support des clés étrangères
--  CSV -> CSV storage engine
-- ...

-- Afficher la liste des tables de la base exemple
SHOW TABLES;

-- Afficher la description de la table vols
DESCRIBE article;

-- Afficher la description de la table articles avec les comentaires et + de détail
SHOW FULL COLUMNS FROM article;

-- Exercice: Gestion de vols
CREATE TABLE pilotes
(
	numero_pilote INT,
	prenom VARCHAR(60),
	nom VARCHAR(60) NOT NULL,
	nombre_heure_vol INT NOT NULL DEFAULT 100
);

CREATE TABLE vols
(
	numero_vol INT,
	heure_depart DATETIME,
	ville_depart VARCHAR(70),
	heure_arrivee DATETIME,
	ville_arrivee VARCHAR(70)
);

CREATE TABLE avion
(
	numero_avion INT,
	modele VARCHAR(40),
	capacit SMALLINT 
);

-- MODIFICATION DES TABLES
-- Renommer une table
RENAME TABLE avion TO avions;

-- Ajouter une colonne à une table
ALTER TABLE avions ADD date_mise_service DATE;

ALTER TABLE avions ADD date_maintance DATE;
-- Supprimer une colonne à une table
ALTER TABLE avions  DROP date_maintance;

-- Changer le type d'une colonne
LTER TABLE articles MODIFY prix DECIMAL(6,2);  -- 6 chiffres au total et 2 chiffres après la virgule max=9999.99 min=-9999.99

-- Renommer une colonne
ALTER TABLE avions CHANGE capacit capacite SMALLINT;

-- Ajouter NOT NULL sur une colonne
ALTER TABLE avions CHANGE capacite capacite SMALLINT NOT NULL;
ALTER TABLE avions CHANGE modele modele VARCHAR(40) NOT NULL;

-- Supprimer NOT NULLsur une colonne
ALTER TABLE avions MODIFY modele VARCHAR(40);

-- Ajouter DEFAULT sur une colonne
ALTER TABLE avions MODIFY capacite SMALLINT NOT NULL DEFAULT 200;

-- Clé primaire
DROP TABLE articles;

CREATE TABLE articles(
	reference INT PRIMARY KEY , 
	description VARCHAR(255),
	prix DOUBLE	 NOT NULL DEFAULT 10.0
);	

-- Ajouter une clé primaire a une table existante
-- ALTER TABLE pilotes ADD 
-- CONSTRAINT PK_pilotes PRIMARY KEY (numero_pilote);

ALTER TABLE pilotes ADD 
CONSTRAINT PRIMARY KEY (numero_pilote);

ALTER TABLE pilotes MODIFY numero_pilote INT AUTO_INCREMENT;

ALTER TABLE vols ADD 
CONSTRAINT PRIMARY KEY(numero_vol);

ALTER TABLE vols MODIFY numero_vol INT AUTO_INCREMENT;

ALTER TABLE avions ADD
CONSTRAINT PRIMARY KEY( numero_avion);
ALTER TABLE avions MODIFY numero_avion INT AUTO_INCREMENT ;


-- Clé étrangères 
-- Relation 1,n
ALTER TABLE vols ADD pilote int;

ALTER TABLE vols ADD
CONSTRAINT FK_pilotes_vols
FOREIGN KEY (pilote)
REFERENCES pilotes(numero_pilote);

ALTER TABLE vols ADD avion int;

ALTER TABLE vols ADD
CONSTRAINT FK_vols_avions
FOREIGN KEY (avion)
REFERENCES  avions (numero_avion);

-- Relation n-n
CREATE TABLE clients
(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50),
	nom VARCHAR (50)
);

CREATE TABLE clients_vols(
	id_client INT,
	num_vol INT,
	
	CONSTRAINT fk_clients_vols
	FOREIGN KEY(id_client)
	REFERENCES clients(id),
	
	CONSTRAINT fk_clients_vols2
	FOREIGN KEY(num_vol)
	REFERENCES vols(numero_vol),
	
	CONSTRAINT PRIMARY KEY (id_client,num_vol) 
);