USE world;
-- Afficher chaque pays et sa capitale
SELECT country.name AS pays, city.name AS capitale FROM country
INNER JOIN city 
ON country.capital= city.id ;

-- Afficher les pays et ville classer par pays puis par ville en ordre alphabétique (ne pas afficher les pays sans ville)
SELECT country.name AS pays, city.name AS ville FROM country
INNER JOIN city
ON country.code = city.country_code
ORDER BY country.name , city.name ; 

-- Afficher les pays sans ville
SELECT country.name AS pays FROM country
LEFT JOIN city
ON country.code= city.country_code
WHERE city.id IS NULL;

SELECT country.name AS pays FROM city
RIGHT JOIN country
ON country.code= city.country_code
WHERE city.id IS NULL;

-- Afficher: les nom pays, langue, pourcentage , classé par pays, pourcentage décroissant
SELECT country.name AS pays, country_language.languages  AS langue, country_language.percentage AS pourcentage
FROM country
INNER JOIN country_language
ON country.code = country_language.country_code
ORDER BY pays,pourcentage DESC ; -- ORDER BY country.name, country_language.percentage DESC

USE elevage;

-- la liste des animaux (leur nom, date de naissance et race) pour lesquels nous n'avons aucune information sur leur pelage
SELECT animal.nom AS nom_animal, date_naissance, race.nom AS race_animal
FROM animal
LEFT JOIN race
ON animal.race_id = race.id 
WHERE race.id IS NULL
OR(NOT race.description LIKE '%robe%' 
AND NOT race.description LIKE '%pelage%' 
AND NOT race.description LIKE '%poil%' 
);
-- la liste des chats et des perroquets amazones, avec leur sexe, leur espèce (nom latin) et leur race s'ils en ont une  
--  Regroupez les chats ensemble, les perroquets ensemble et, au sein de l'espèce, regroupez les races
SELECT animal.nom AS nom_animal , animal.sexe, espece.nom_latin AS nom_espece, Race.nom AS race_nom 
FROM animal
INNER JOIN espece
ON animal.espece_id = espece.id
LEFT JOIN race
ON animal.race_id =race.id 
WHERE espece.nom_courant IN('chat','perroquet amazone')
ORDER BY espece.nom_courant, Race.nom;