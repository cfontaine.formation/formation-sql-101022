USE bibliotheque;

SELECT ceil(1.4) AS ceil,floor(1.4)  AS floor, round (1.33333888,3) AS round , truncate(1.33333888,3);

SELECT id, titre, annee FROM Livres ORDER BY rand() LIMIT 5; 

SELECT nom,char_length(nom) FROM Auteurs;

SELECT ascii('a');

SELECT concat(prenom, ' ', nom) AS auteur FROM auteurs;

SELECT concat_ws(' ',id,prenom, nom) AS auteur FROM auteurs;

SELECT INSERT(prenom,2,2,nom) FROM auteurs;

SELECT REPLACE(nom,'er','--')FROM auteurs;

SELECT REPEAT('er',5);

SELECT REVERSE('Bonjour');

SELECT LEFT(prenom,3), RIGHT (prenom,3), Substr(nom,2,3) FROM auteurs;

SELECT TRIM('        Hello world          '); -- hello world

SELECT LTRIM('        Hello world          '); 

SELECT RTRIM('        Hello world          '); 

SELECT STRCMP('bonjour','hello'), STRCMP('hello', 'bonjour'),STRCMP('aaa','aaa');

SELECT POSITION('o' in 'hello world');

SELECT FIND_IN_SET('aze','rtf,dfgh,feybj,aze,123');

SELECT FIELD('aze','rtf','dfgh','feybj','aze','123');

SELECT current_timestamp(), current_date(), current_time(); 

SELECT date(current_timestamp());

SELECT titre ,YEAR (current_date())- annee AS age FROM livres ORDER BY age;

SELECT datediff('2023-01-01',current_date());

SELECT date_add(current_date(), INTERVAL 1 MONTH);

SELECT date_format(current_date(),'%d/%m/%y');

-- FOnction d'agrégation
SELECT count(id) AS nb_livre FROM livres;

SELECT count(id) AS nb_livre FROM livres WHERE annee=1992;

SELECT MAX(annee), MIN(annee), ROUND(AVG(annee),0),MAX(titre) FROM livres;

SELECT COUNT(DISTINCT annee) , COUNT(annee) FROM livres;

-- autres fontions
SELECT COALESCE(NULL,NULL,4,NULL, 'az');

SELECT CURRENT_USER(),DATABASE(),VERSION();

SELECT annee,
CASE 
	WHEN annee>2000 THEN 'Moderne'
	WHEN annee<2000 AND annee>1900 THEN '20 siecle'
	ELSE 'livre ancien'
END
FROM livres;
END

