USE exemple;

-- Insertion de données => INSERT
-- Insérer une ligne en spécifiant toutes les colonnes

INSERT INTO articles VALUES(1,'TV 4K',500.0);

-- Insérer une ligne en spécifiant les colonnes souhaitées
INSERT INTO articles(prix,reference,description) VALUES (350.0,2,'Smartphone android');

INSERT INTO articles (reference,description) VALUES (3,'Clavier azerty');

INSERT INTO articles (reference,prix) VALUES (4,34.0);

-- Insérer plusieurs lignes à la fois
INSERT INTO articles(reference,description,prix) VALUES
(5,'Souris gamer',50.0),
(6,'Ordinateur portable',950.0),
(7,'Cable RJ45 5m',20.0);

INSERT INTO avions(modele,capacite,date_mise_service) VALUES ('Airbus A320',400,'2005-10-23');
INSERT INTO pilotes(prenom, nom,nombre_heure_vol) VALUES('John','Doe',400);
INSERT INTO vols(heure_depart,ville_depart,heure_arrivee,ville_arrivee,pilote,avion)
VALUES ('2005-10-23 18:00:00','Lille','2005-10-23 19:00:00','Bordeaux',1,1);

-- Supprimer des données => DELETE, TRUNCATE

-- Supression de la ligne qui a pour reference 4 dans la table articles
DELETE FROM articles WHERE reference = 4;

-- Supression de toutes les lignes qui ont un prix inférieur à 40.0
DELETE FROM articles WHERE prix<40.0;

-- Supression de toutes les lignes de la table articles
DELETE FROM articles;
 -- Supression de toutes les lignes de la table vols et avions => ne ré-initialise pas la colonne AUTO_INCREMENT
DELETE FROM vols;
DELETE FROM avions;

SET FOREIGN_KEY_CHECKS =0;  -- désactiver la vérification des clés étrangères (MYSQL/ MARIADB)
-- Supression de toutes les lignes de la table avions  => remet les colonnes AUTO_INCREMENT à 0
TRUNCATE TABLE avions;
SET FOREIGN_KEY_CHECKS =1; --réactiver la vérification des clés étrangères

-- Modification des données => UPDATE
-- Modification de la description de l'article qui a pour reference 1
UPDATE articles SET description = 'TV HD' WHERE reference=1;

-- Modification de toutes les lignes de table articles qui ont un prix <20.0 => nouvelle valeur 21.0
UPDATE articles SET prix=21.0 WHERE prix<20.0;

-- Modification de tous les prix > 100.0 augmentation de 10%
UPDATE articles SET prix=prix*1.1 WHERE prix>100.0;

-- Chargement d'un fichier binaire => LOAD_FILE
USE pizzeria;
INSERT INTO pizzas(nom,base,prix,photo) VALUES("pizza aux 4 fromages","Rouge",15.0,LOAD_FILE('C:/Dawan/pizza.jpg'));