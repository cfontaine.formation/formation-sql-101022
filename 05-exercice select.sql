USE world;
-- Afficher le nom des pays qui pour continents : North America
SELECT name FROM country WHERE continent ='North America';

-- Afficher le code du pays et la langue dont les langues officielles sont parlées plus de 50%
SELECT country_code, languages FROM country_language WHERE is_official='T' AND percentage >50 ; 

-- Afficher les villes qui ont une population comprise entre 50000 et 100000
SELECT name , population FROM city WHERE population BETWEEN 50000 AND 100000;

-- Afficher les villes qui font partie des district Noord-Holland, Groningen , Zuid-Holland ,Limburg
SELECT name  FROM city WHERE district IN ('Noord-Holland', 'Groningen' , 'Zuid-Holland' ,'Limburg');

-- Afficher la ville la plus peuplé
SELECT name FROM city ORDER BY  population DESC LIMIT 1; 
-- Afficher les 10 premiers pays classé par ordre alphabétiques
SELECT name FROM city ORDER BY name LIMIT 10;

-- Afficher les 10 pays les plus peuplés (nom et population)
SELECT name, population FROM country ORDER BY population DESC LIMIT 10;  
