USE bibliotheque;
-- requete imbriqué qui retourne un seul résultat => where ou having
SELECT count(id) FROM livres l WHERE annee=(SELECT round(AVG(annee),0) FROM livres);


-- SELECT titre,annee FROM livres WHERE annee IN (SELECT DISTINCT annee FROM livres ORDER BY rand () LIMIT 3);

USE elevage;
 -- in
SELECT * FROM animal
WHERE espece_id IN(
 SELECT id FROM espece WHERE nom_courant LIKE 'ch%'); 

-- exist
SELECT * FROM race 
WHERE EXISTS(
SELECT id FROM animal WHERE nom='rox');

-- ALL
SELECT id, nom, espece_id  FROM animal
WHERE espece_id= ALL(SELECT id FROM espece WHERE nom_courant IN('Tortue d''Hermann','Perroquet amazone'));

-- ANY
SELECT id, nom, espece_id  FROM animal
WHERE espece_id= ANY(SELECT id FROM espece WHERE nom_courant IN('Tortue d''Hermann','Perroquet amazone'));

