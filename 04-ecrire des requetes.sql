-- Sélectionner la base des données bibliotheque
USE bibliotheque;

SELECT prenom,nom FROM auteurs;

-- Quand il y a une ambiguité sur le nom d'une colonne, on ajoute le nom de la table  table.colonne
-- pour différentier les 2 colonnes
SELECT prenom,auteurs.nom,pays.nom FROM auteurs , pays;

-- * -> Obtenir toutes les colonnes d’un tablea
SELECT * FROM auteurs;

-- On peut mettre dans les colonnes d'un SELECT une constante ou une colonne qui provient d'un calcul
SELECT titre,'age=',2022-annee FROM livres;

-- ALIAS
-- AS permet de spécifier un Alias pour le nom d'un colonne ou d'une table

-- sur les colonnes
SELECT titre,2022-annee AS age FROM livres;
SELECT titre,2022-annee age FROM livres;

SELECT prenom,auteurs.nom AS nom_auteur,pays.nom AS nation FROM auteurs , pays;
SELECT titre AS titre_livre,2022-annee age FROM livres;

-- sur les tables
SELECT nom FROM genres AS categories;
SELECT nom FROM genres g; 

-- DISTINCT -> permet d’éviter les doublons dans les résultats d’un SELECT
SELECT prenom FROM auteurs;
SELECT DISTINCT prenom FROM auteurs;

-- La clause WHERE permet de sélectionner des lignes qui respectent une condition
-- Selection de tous les titres de livre qui sont sortie après 2000
SELECT titre, annee FROM livres WHERE annee>2000;

-- Selection de tous les auteurs américains (nation=1)
SELECT prenom,nom FROM auteurs WHERE nation=1;

-- Les opérateurs logiques AND et OR permettent de combiner des conditions
-- Selection des titres, de l'année de sortie du livre qui sont sorties entre 1970 et 1980
SELECT titre , annee FROM livres WHERE annee>1970 AND annee<1980;

-- Selection des titres, de l'année de sortie du livre qui sont sorties en 1954 et en 1992
SELECT titre, annee FROM livres WHERE annee=1954 OR annee=1992;

-- L'opérateur NOT inverse le resultat
-- Selection des titres, de l'année de sortie du livre qui sont sortie sauf en 1992 (Condition inversée)
SELECT titre, annee FROM livres WHERE NOT annee=1992; 

-- L'opérateur XOR ou exclusif 
SELECT prenom, nom FROM auteurs WHERE prenom='Georges' OR nation=9;

-- Sélection du noms et du prénoms  des auteurs belges ou des auteurs qui ont pour prénom Georges mais pas des auteurs qui respect les 2 conditions
SELECT prenom, nom FROM auteurs WHERE prenom='Georges' XOR nation=9;

-- Sélection des auteurs qui ont pour prénom pierre ou john et qui sont nait après le 1er janvier 1930
SELECT prenom, nom FROM auteurs WHERE (prenom='Pierre' OR prenom='john') AND naissance >'1930-01-01';

-- IS NULL permet de tester si une valeur est égal à NULL
-- IS NOT NULL  permet de tester si une valeur est différente de NULL
-- Sélection de tous les colonnes de la table auteurs pour les auteurs qui sont vivants => deces = NULL
SELECT prenom,nom FROM auteurs WHERE deces IS NULL;

-- Sélection de tous les colonnes de la table auteurs pour les auteurs qui sont décédés 
SELECT prenom,nom FROM auteurs WHERE deces IS NOT NULL;

-- Sélection du titre et de l'année pour les livres sortie en 1920,1965,1982 ou 1992
SELECT titre, annee  FROM livres WHERE annee IN(1920,1992,1965,1982);

-- Sélection des titres des livres qui sortie entre 1950 et 1985 
SELECT titre,annee FROM livres WHERE annee BETWEEN 1950 AND 1985; 

-- Sélection du prénom et du nom des auteurs qui sont nés entre le 1er janvier 1930 et  le 1er janvier 1948
SELECT prenom,nom,naissance FROM auteurs WHERE naissance BETWEEN '1930-01-01' AND '1948-01-01';

SELECT prenom,nom FROM auteurs WHERE prenom BETWEEN 'John' AND 'Pierre';

-- Avec LIKE % représente 0,1 ou plusieurs caratères inconnues
--           _ représente un caratère inconnue
-- Sélection du titre des livres qui commence par D et qui fait 4 caractères (ne tient pas compte de la casse)
SELECT titre FROM livres WHERE titre LIKE 'd___';

SELECT titre FROM livres WHERE titre LIKE 'd___%';

-- Sélection du prénoms pour les auteurs qui ont un prénom composé
SELECT prenom,nom FROM auteurs WHERE prenom LIKE '%-%';

-- Selection de tous les livres triés par rapport à leur titre par ordre alphabétique
SELECT * FROM livres ORDER BY titre;

-- Sélection des titres des livres qui sont sortie entre 1970 et 1979 triés par année croissante et titre par ordre aplphabéthique
SELECT titre,annee FROM livres WHERE annee BETWEEN 1970 AND 1979 ORDER BY annee,titre;

-- Selection de tous les auteurs décédés trié par leur date de naissance décroissante, leur nom (croissant par défaut) et leur prénom décroissant
SELECT prenom, nom, naissance FROM auteurs WHERE deces IS NOT NULL ORDER BY naissance DESC, nom , prenom DESC;

-- sélectionner les 5 auteurs les plus vieux
SELECT prenom,nom FROM auteurs ORDER BY naissance LIMIT 5;

-- sélectionner les 5 auteurs les plus vieux à partir du 3ème
SELECT prenom,nom FROM auteurs ORDER BY naissance LIMIT 5 OFFSET 2;

-- idem autre syntaxe (MySQL)
SELECT prenom,nom FROM auteurs ORDER BY naissance LIMIT 2,5;

